package com.experiment.exp.api;

import com.experiment.exp.model.Movie;
import com.experiment.exp.service.MovieService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RequestMapping("movie")
@RestController
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class MovieController {
    private final MovieService movieService;

    @PostMapping
    public void addMovie(@RequestBody Movie movie){
        movieService.addMovie(movie);
    }

    @GetMapping
    public List<Movie> getAllMovies(){
        return movieService.getAllMovies();
    }

    @GetMapping(path = "{id}")
    public  Movie getMovieById(@PathVariable("id") UUID id) {
        return movieService.getMovieById(id).orElse(null);
    }

}
