package com.experiment.exp.dao;

import com.experiment.exp.model.Movie;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository("mocking")
public class MockingMovieDataAccessService implements MovieDao {

    private static List<Movie> mockDB = new ArrayList<>();

    @Override
    public boolean insertMovie(UUID id, Movie movie) {
        mockDB.add(new Movie(id, movie.getName()));
        return true;
    }

    @Override
    public List<Movie> selectAllMovies() {
        return mockDB;
    }

    @Override
    public boolean deleteMovieById(UUID id) {


        return false;
    }

    @Override
    public Optional<Movie> selectMovieById(UUID id) {
        return mockDB.stream().filter( movie -> movie.getId().equals(id)).findFirst();
    }


    @Override
    public boolean updateMovieById(UUID id, Movie movie) {
        return false;
    }
}
