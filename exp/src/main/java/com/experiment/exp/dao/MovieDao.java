package com.experiment.exp.dao;

import com.experiment.exp.model.Movie;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface MovieDao {
    boolean insertMovie(UUID id, Movie movie);

    default boolean insertMovie(Movie movie){
        UUID id = UUID.randomUUID();
        return  insertMovie(id, movie);
    }

    List<Movie> selectAllMovies();

    boolean deleteMovieById(UUID id);
    Optional<Movie> selectMovieById(UUID id);
    boolean updateMovieById(UUID id, Movie movie);

}
