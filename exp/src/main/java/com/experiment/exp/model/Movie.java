package com.experiment.exp.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@AllArgsConstructor
@Getter
@Setter
public class Movie {

    @JsonProperty("id") private final UUID id;
    @JsonProperty("name") private final String name;


}
