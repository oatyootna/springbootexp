package com.experiment.exp;

import com.experiment.exp.model.Movie;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExpApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExpApplication.class, args);
	}

}
