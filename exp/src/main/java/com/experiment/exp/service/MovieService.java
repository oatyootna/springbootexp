package com.experiment.exp.service;

import com.experiment.exp.dao.MovieDao;
import com.experiment.exp.model.Movie;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;


@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class MovieService {

    @Qualifier("mocking") private final MovieDao movieDao;

    public boolean addMovie(Movie movie){
        return movieDao.insertMovie(movie);
    }

    public List<Movie> getAllMovies(){
        return movieDao.selectAllMovies();
    }

    public Optional<Movie> getMovieById(UUID id){
        return movieDao.selectMovieById(id);
    }
}
